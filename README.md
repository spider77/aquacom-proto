# AquaCom-Prototype

The idea of the project is to use an arduino to control different devices around an aquarium. This is a prototype for experiments to check the possibilities.

## Used Libraries

- https://codebender.cc/library/ShiftLCD
- https://github.com/adidax/dht11
- https://github.com/JChristensen/DS3232RTC
- https://github.com/milesburton/Arduino-Temperature-Control-Library

## Licence

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3.0 as published by the Free Software Foundation.
