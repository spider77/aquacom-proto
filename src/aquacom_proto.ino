#include <ShiftLCD.h>
#include <Wire.h>
#include <DS3231.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <dht11.h>
#include <EEPROM.h>

#define ONE_WIRE_BUS 10

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

ShiftLCD lcd(2, 4, 3);

DS3231 clock;
RTCDateTime dt;

dht11 DHT11;
#define DHT11PIN 11

short myPins[5] = {5, 6, 7, 8, 9};
short pinValues[5][8];

byte byteRead;
String str;
 
void setup() {
  Serial.begin(9600);
  lcd.begin(16, 2);
  clock.begin();
  sensors.begin();
  sensors.setResolution( 12 );
  
  for (int i = 0; i < 5; i = i + 1) {
    pinMode(myPins[i], OUTPUT);
    digitalWrite(myPins[i], HIGH);
  }

  setupTimerValues();
}
 
void loop() {
  if (Serial.available()) {
    str = Serial.readStringUntil('\n');
    setTimer(str);
  }
  
  float temperatur = 0;
  char stringbuffer[ 17];

  int chk = DHT11.read(DHT11PIN);
  
  dt = clock.getDateTime();
  
  lcd.setCursor(0, 0);
  lcd.print((int)DHT11.temperature);
  lcd.setCursor(3, 0);
  lcd.print((int)DHT11.humidity);
  lcd.setCursor(8, 0);
  lcd.print(clock.dateFormat("H:i:s", dt));

  sensors.requestTemperatures();
  temperatur = sensors.getTempCByIndex(0);

  lcd.setCursor(0, 1);
  lcd.print(dtostrf( temperatur, 5, 2, stringbuffer ));
  lcd.setCursor(6, 1);
  lcd.print(clock.dateFormat("d-m-Y", dt));

  for (int i = 0; i < 5; i = i + 1) {
    setPin(i, dt.hour, dt.minute);  
  }

  sendSensorDataToSerial(temperatur, (int)DHT11.temperature, (int)DHT11.humidity);
  sendTimeDataToSerial();
  
  delay(1000);
}

// setzen von Relai-Pins
void setPin(short pin, short hour, short minute) {
  int i = 0;

  if(pinValues[pin][0] < 24){
    if(((hour > pinValues[pin][0]) || (hour == pinValues[pin][0] && minute >= pinValues[pin][1])) && ((hour < pinValues[pin][2]) || (hour == pinValues[pin][2] && minute < pinValues[pin][3]))) i = 1;
  }

  if(pinValues[pin][4] < 24){
    if(((hour > pinValues[pin][4]) || (hour == pinValues[pin][4] && minute >= pinValues[pin][5])) && ((hour < pinValues[pin][6]) || (hour == pinValues[pin][6] && minute < pinValues[pin][7]))) i = 1;
  }
    
  if(i == 1){
    digitalWrite(myPins[pin], LOW);
  }
  else{
    digitalWrite(myPins[pin], HIGH);
  }
}


// ausgabe der Werte - im Echtbetrieb nicht notwendig
void sendTimeDataToSerial(){
  Serial.println("");
  for (short i = 0; i < 5; i = i + 1) {
    sendDataForPin(i);
  }
}

// ausgabe der Werte - im Echtbetrieb nicht notwendig
void sendDataForPin(short i){
  Serial.print("Pin "); Serial.print(myPins[i]);
  Serial.print(": an 1 "); Serial.print(pinValues[i][0]); Serial.print(":"); Serial.print(pinValues[i][1]);
  Serial.print(", aus 1 "); Serial.print(pinValues[i][2]); Serial.print(":"); Serial.print(pinValues[i][3]);
  Serial.print(": an 2 "); Serial.print(pinValues[i][4]); Serial.print(":"); Serial.print(pinValues[i][5]);
  Serial.print(", aus 2 "); Serial.print(pinValues[i][6]); Serial.print(":"); Serial.print(pinValues[i][7]);
  Serial.println("");
}

// Werte auseinander nehmen und in eprom schreiben
void setTimer(String content){
  int pinIndex, onh1, onm1, offh1, offm1, onh2, onm2, offh2, offm2 = -1;

  onh1 = onm1 = offh1 = offm1 = onh2 = onm2 = offh2 = offm2 = -1;
  int n = sscanf(content.c_str(), "%d;%d:%d,%d:%d,%d:%d,%d:%d,%d:%d", &pinIndex, &onh1, &onm1, &offh1, &offm1, &onh2, &onm2, &offh2, &offm2);

  Serial.println("");  
  if(pinIndex >= 0 && pinIndex < 5){
    EEPROM.write(pinIndex * 7 + 0 + pinIndex, getHourValue(onh1));
    EEPROM.write(pinIndex * 7 + 1 + pinIndex, getMinuteValue(onm1));
    EEPROM.write(pinIndex * 7 + 2 + pinIndex, getHourValue(offh1));
    EEPROM.write(pinIndex * 7 + 3 + pinIndex, getMinuteValue(offm1));
    EEPROM.write(pinIndex * 7 + 4 + pinIndex, getHourValue(onh2));
    EEPROM.write(pinIndex * 7 + 5 + pinIndex, getMinuteValue(onm2));
    EEPROM.write(pinIndex * 7 + 6 + pinIndex, getHourValue(offh2));
    EEPROM.write(pinIndex * 7 + 7 + pinIndex, getMinuteValue(offm2));
    setupTimerValues();
  }
  else {
    Serial.println("Error:");  
  }
}

short getHourValue(int hour){
  if(hour >= 0 && hour < 24){
    return (short)hour;
  }
  else{
    return 25;
  }
}

short getMinuteValue(int minute){
  if(minute >= 0 && minute < 60){
    return (short)minute;
  }
  else{
    return 0;
  }
}

void setupTimerValues(){
  for (int i = 0; i < 5; i++){
    for (int j = 0; j < 8; j++){
      int adr = i * 7 + j + i;
      pinValues[i][j] = EEPROM.read(adr);
      adr++;
    }
  }
}

// Ausgabe Zeit/Datum - für Echbetrieb nicht notwendig
void sendSensorDataToSerial(float tempIn, int tempOut, int humidity){
  Serial.println("");
  Serial.print("TIn: "); Serial.print(tempIn); Serial.print(", TOut: "); Serial.print(tempOut); Serial.print(", Hum.: "); Serial.print(humidity);
  Serial.println("");
}

